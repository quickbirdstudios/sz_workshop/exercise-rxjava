package com.quickbirdstudios.exerciserxjava.model;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class Cafe {
    private String name;
    private float rating;

    public Cafe(String name, float rating) {
        this.name = name;
        this.rating = rating;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Cafe(name " + name + " / rating: " + rating + ")";
    }
}
