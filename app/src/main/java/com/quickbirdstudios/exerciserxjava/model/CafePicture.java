package com.quickbirdstudios.exerciserxjava.model;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class CafePicture {
    private final String pictureData;

    public CafePicture(String pictureData) {
        this.pictureData = pictureData;
    }

    public String getPictureData() {
        return pictureData;
    }
}
