package com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui;

import android.arch.lifecycle.ViewModel;

import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.service.UserService;
import com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.utils.DependencyInjector;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Malte Bucksch on 10/04/2018.
 */
public class LoginViewModelImpl extends ViewModel implements LoginViewModel {
    private UserService userService = DependencyInjector.injectUserService();

    private BehaviorSubject<String> emailText = BehaviorSubject.createDefault("");
    private BehaviorSubject<String> passwordText = BehaviorSubject.createDefault("");
    private PublishSubject<Boolean> inputLoginTrigger = PublishSubject.create();

    @Override
    public Subject<String> inputEmailText() {
        return emailText;
    }

    @Override
    public Subject<String> inputPasswordText() {
        return passwordText;
    }

    @Override
    public Subject<Boolean> inputLoginTrigger() {
        return inputLoginTrigger;
    }


    /***
     *     HINT: you can also check out the TranslatorViewModelImpl for reference
     *     {@link com.quickbirdstudios.exerciserxjava.exercises.exercise6_mvvm_ui.example_of_translator.TranslatorViewModelImpl }
     *
     *    IMPORTANT: NO "subscribe" or imperative code here (!).
     *    Simply return an Input with some operators applied to it (return inputX.map {})
     */
    @Override
    public Observable<Boolean> outputIsPasswordValid() {
        // TODO 1: return observable representing the state if the password is currently valid

        // add solution here:
        return null;
    }

    @Override
    public Observable<Boolean> outputIsEmailValid() {
        // TODO 2: return observable representing the state if the email is currently valid

        // add solution here:
        return null;
    }

    @Override
    public Observable<Boolean> outputIsLoginButtonEnabled() {
        // TODO 3: return observable representing the state if
        // TODO 3: BOTH the password and email are currently valid
        // HINT: combineLatest

        // solution
        return null;
    }

    @Override
    public Observable<Boolean> outputLoggedInTrigger() {
        // TODO 4 EXTRA (difficult): if the Login-Button was clicked, trigger the "login(email,password)"-
        // TODO 4 EXTRA (difficult): function from the UserService

        // TODO 5 EXTRA (difficult): if the UserService would return an error, this whole thing SHALL NOT CRASH!
        // TODO 5 EXTRA (difficult): jump to the UserServiceImpl, return an Error for testing this and
        // TODO 5 EXTRA (difficult): make sure HERE that you handle the error gracefully

        return inputLoginTrigger(); // <- add solution here by applying operators

    }

}