package com.quickbirdstudios.exerciserxjava.exercises.exercise2_creation_subscription;

import android.annotation.SuppressLint;

import com.quickbirdstudios.exerciserxjava.util.ExecutionUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/**
 * Created by Malte Bucksch on 15/06/2018.
 */
public class CreationSubscriptionExercise4 {
    public static void main(String[] args) {
        printIntervalRythm();

        ExecutionUtils.keepProgramAliveForever();
    }

    @SuppressLint("CheckResult")
    private static void printIntervalRythm() {
        // TODO 4 (EXTRA): print out alternating ticks/tocks every second using "interval"
        // example-ouput: tick, tock!, tick, tock!, tick ...






    }
}
